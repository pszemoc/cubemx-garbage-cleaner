#pragma once

#include "thread_base.hpp"

class ${thread.name}_thread_t : public thread_base_t<${thread.name}_thread_t, ${thread.stack_size}> {
private:
  static constexpr char             name[]            = "${thread.name}";
  static constexpr osPriority_t     priority          = ${thread.priority};

% for queue in thread.queues:
  osMessageQueueId_t                ${queue.name}_queue          = nullptr;
  static constexpr uint8_t          ${queue.name}_queue_size     = ${queue.size};
  static constexpr uint16_t         ${queue.name}_item_size      = ${queue.item_size};
  uint8_t                           ${queue.name}_queue_buffer[${queue.name}_queue_size * ${queue.name}_item_size];
  osStaticMessageQDef_t             ${queue.name}_queue_control_block;
  
% endfor

public:
  ${thread.name}_thread_t();
  void thread();
  %for queue in thread.queues:
  osMessageQueueId_t get_${queue.name}_queue() const ;
  %endfor
  const char * get_name() const;
  const osPriority_t get_priority() const;
};
