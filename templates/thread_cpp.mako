#include "${thread.name}_thread.hpp"

${thread.name}_thread_t::${thread.name}_thread_t() {
% for queue in thread.queues:
  const osMessageQueueAttr_t ${queue.name}_queue_attr = {
    .name = "${queue.name}_queue",
    .attr_bits = 0,
    .cb_mem = &${queue.name}_queue_control_block,
    .cb_size = sizeof(${queue.name}_queue_control_block),
    .mq_mem = &${queue.name}_queue_buffer,
    .mq_size = sizeof(${queue.name}_queue_buffer)
  };
  ${queue.name}_queue = osMessageQueueNew(${queue.name}_queue_size, ${queue.name}_item_size, &${queue.name}_queue_attr);

% endfor
}

void ${thread.name}_thread_t::thread() {
  for (;;) {
    osDelay(1);
  }
}

% for queue in thread.queues:
osMessageQueueId_t ${thread.name}_thread_t::get_${queue.name}_queue() const {
  return ${queue.name}_queue;
}

% endfor
const char * ${thread.name}_thread_t::get_name() const {
  return name;
}

const osPriority_t ${thread.name}_thread_t::get_priority() const {
  return priority;
}

