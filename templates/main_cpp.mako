/* Includes ------------------------------------------------------------------*/
#include "cmsis_os.h"
#include "peripherals.hpp"
% for thread in threads:
#include "${thread.name}_thread.hpp"
% endfor

% for thread in threads:
${thread.name}_thread_t ${thread.name}_thread;
% endfor

/* Private function prototypes -----------------------------------------------*/

void *operator new(size_t size) {
  return pvPortMalloc(size);
}

void *operator new[](size_t size) {
  return pvPortMalloc(size);
}

void operator delete(void *ptr) {
  vPortFree(ptr);
}

void operator delete[](void *ptr) {
  vPortFree(ptr);
}

int main(void) {
  peripherals::init();

  osKernelInitialize();

% for thread in threads:
  ${thread.name}_thread.start();
% endfor

  osKernelStart();

  while (1) {}
}