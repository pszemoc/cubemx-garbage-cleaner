#pragma once

#include "stm32${mcu_family}xx_hal.h"
#include "mem_ptr.hpp"

namespace peripherals {
// pins & ports definitions

% for gpio in gpios:
constexpr uint16_t    ${gpio.name}_pin    = ${gpio.pin};
constexpr auto        ${gpio.name}_port   = mem_ptr_t<GPIO_TypeDef>(${gpio.port}_BASE);
% endfor

// peripherals handlers
% for peripheral in peripherals:
extern ${peripheral};
% endfor

// init functions
void init();
void system_clock_config();
% for init in peripherals_init:
void ${init}();
% endfor
void error_handler();
}


