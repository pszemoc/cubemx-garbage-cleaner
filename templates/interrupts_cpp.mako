#include "interrupts.hpp"
#include "peripherals.hpp"

using namespace peripherals;

//////////// FAULT HANDLERS ///////////

% for handler in fault_handlers:
${handler}
% endfor

//////////// IRQ HANDLERS ////////////

% for handler in irq_handlers:
${handler}
% endfor

//////////// CALLBACKS ////////////////

% for callback in callbacks:
${callback}
% endfor
