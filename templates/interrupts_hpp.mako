#pragma once

#ifdef __cplusplus
extern "C" {
#endif

% for handler in handlers: 
${handler}
% endfor 

#ifdef __cplusplus
}
#endif

