//
// Created by pszemo on 06.12.2019.
//

#pragma once

#include <cstdint>
#include "cmsis_os.h"

typedef StaticTask_t osStaticThreadDef_t;
typedef StaticQueue_t osStaticMessageQDef_t;
typedef StaticSemaphore_t osStaticMutexDef_t;
typedef StaticSemaphore_t osStaticSemaphore_t;
typedef StaticTimer_t osStaticTimerDef_t;

template <class T, std::size_t stack_size>
class thread_base_t {
private:
  uint8_t thread_stack[stack_size];
  osStaticThreadDef_t thread_control_block;

protected:
  osThreadId_t thread_id;

public:
  void start() {
    T& derived = static_cast<T&>(*this);
    const osThreadAttr_t attributes = {
      .name = derived.get_name(),
      .attr_bits = 0,
      .cb_mem = &thread_control_block,
      .cb_size = sizeof(thread_control_block),
      .stack_mem = &thread_stack[0],
      .stack_size = sizeof(thread_stack),
      .priority = derived.get_priority()
    };
    thread_id = osThreadNew(thread, &derived, &attributes);
  }
  static void thread(void* args) {
    T* derived = static_cast<T*>(args);
    derived->thread();
  }
};

