#include "peripherals.hpp"

namespace peripherals{
% for peripheral in peripherals:
  ${peripheral};
% endfor
}

using namespace peripherals;

void peripherals::init() {
  HAL_Init();

  system_clock_config();

% for init in peripherals_init:
  ${init}();
% endfor
}

//////////// PERIPHERALS INIT //////////////

${system_clock_config}

% for peripheral_init_func in peripherals_init_func:
${peripheral_init_func}
% endfor

////////////  ERROR HANLDERS ///////////////

void peripherals::error_handler(void) {}

////////////  TICK /////////////////////////

% for function in timebase_func:
${function}
% endfor

///////////// MSP ////////////////////////

% for func in msp_init:
${func}
% endfor