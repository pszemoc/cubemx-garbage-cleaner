import os
import re
import urllib.request
import argparse
import subprocess
from mako.template import Template
from shutil import copyfile, rmtree, copyfileobj

parser = argparse.ArgumentParser()
parser.add_argument('path', help='Path to CubeMX Project.', default='')

template_files = [
  'CMakeLists.mako',
  'interrupts_cpp.mako',
  'interrupts_hpp.mako',
  'main_cpp.mako',
  'mem_ptr.hpp',
  'peripherals_hpp.mako',
  'peripherals_cpp.mako',
  'thread_base.hpp',
  'thread_hpp.mako',
  'thread_cpp.mako'
]

priorities = dict([
  (0           , 'osPriorityNone'          ), 
  (1           , 'osPriorityIdle'          ), 
  (8           , 'osPriorityLow'           ), 
  (8+1         , 'osPriorityLow1'          ), 
  (8+2         , 'osPriorityLow2'          ), 
  (8+3         , 'osPriorityLow3'          ), 
  (8+4         , 'osPriorityLow4'          ), 
  (8+5         , 'osPriorityLow5'          ), 
  (8+6         , 'osPriorityLow6'          ), 
  (8+7         , 'osPriorityLow7'          ), 
  (16          , 'osPriorityBelowNormal'   ), 
  (16+1        , 'osPriorityBelowNormal1'  ), 
  (16+2        , 'osPriorityBelowNormal2'  ), 
  (16+3        , 'osPriorityBelowNormal3'  ), 
  (16+4        , 'osPriorityBelowNormal4'  ), 
  (16+5        , 'osPriorityBelowNormal5'  ), 
  (16+6        , 'osPriorityBelowNormal6'  ), 
  (16+7        , 'osPriorityBelowNormal7'  ), 
  (24          , 'osPriorityNormal'        ), 
  (24+1        , 'osPriorityNormal1'       ), 
  (24+2        , 'osPriorityNormal2'       ), 
  (24+3        , 'osPriorityNormal3'       ), 
  (24+4        , 'osPriorityNormal4'       ), 
  (24+5        , 'osPriorityNormal5'       ), 
  (24+6        , 'osPriorityNormal6'       ), 
  (24+7        , 'osPriorityNormal7'       ), 
  (32          , 'osPriorityAboveNormal'   ), 
  (32+1        , 'osPriorityAboveNormal1'  ), 
  (32+2        , 'osPriorityAboveNormal2'  ), 
  (32+3        , 'osPriorityAboveNormal3'  ), 
  (32+4        , 'osPriorityAboveNormal4'  ), 
  (32+5        , 'osPriorityAboveNormal5'  ), 
  (32+6        , 'osPriorityAboveNormal6'  ), 
  (32+7        , 'osPriorityAboveNormal7'  ), 
  (40          , 'osPriorityHigh'          ), 
  (40+1        , 'osPriorityHigh1'         ), 
  (40+2        , 'osPriorityHigh2'         ), 
  (40+3        , 'osPriorityHigh3'         ), 
  (40+4        , 'osPriorityHigh4'         ), 
  (40+5        , 'osPriorityHigh5'         ), 
  (40+6        , 'osPriorityHigh6'         ), 
  (40+7        , 'osPriorityHigh7'         ), 
  (48          , 'osPriorityRealtime'      ), 
  (48+1        , 'osPriorityRealtime1'     ), 
  (48+2        , 'osPriorityRealtime2'     ), 
  (48+3        , 'osPriorityRealtime3'     ), 
  (48+4        , 'osPriorityRealtime4'     ), 
  (48+5        , 'osPriorityRealtime5'     ), 
  (48+6        , 'osPriorityRealtime6'     ), 
  (48+7        , 'osPriorityRealtime7'     ), 
  (56          , 'osPriorityISR'           ), 
  (-1          , 'osPriorityError'         ), 
  (0x7FFFFFFF  , 'osPriorityReserved'      )
])

mcu = None
threads = []

def cmake_preprocessor(source):
    source = re.sub(r"\${(.+?)}", r"${'${'}\1${'}'}", source)
    source = re.sub(r"\^\[(.+?)\]", r"${\1}", source)
    return source

class Mcu:
  def __init__(self, name):
    self.full_name = name
    self.short_name = name[5:9]
    self.family = name[5:7]

class Gpio:
  def __init__(self, name, pin, port):
    self.name = name
    self.pin = pin
    self.port = port

class Queue:
  def __init__(self, name, size, item_size):
    self.name = name
    self.size = size
    self.item_size = item_size

class Thread:
  def __init__(self, name, stack_size, priority):
    self.name = name
    self.stack_size = stack_size
    self.priority = priorities[int(priority)]
    self.queues = []

def remove_project_files(working_directory):
  try:
    os.remove(working_directory+'.cproject')
    os.remove(working_directory+'.mxproject')
    os.remove(working_directory+'.project')
  except FileNotFoundError:
    pass

def find_ioc_file(working_directory):
  for file_name in os.listdir(working_directory):
    if 'ioc' in file_name:
      return file_name

def parse_mcu(file):
  global mcu
  for line in file:
    if 'Mcu.Name' in line:
      mcu = Mcu(line.split('=')[1])
  file.seek(0)

def parse_threads(file):
  global threads
  queues = []
  queues_unparsed = []
  threads_line = ''
  queues_line = ''
  for line in file:
    if 'FREERTOS.Queues' in line:
      queues_line = line
    elif 'FREERTOS.Tasks' in line:
      threads_line = line
  threads_unparsed = (threads_line.split('=')[1]).split(';')
  if queues_line != '':
    queues_unparsed = (queues_line.split('=')[1]).split(';') 
  for t in threads_unparsed:
    splitted = t.split(',')
    threads.append(Thread(splitted[0], splitted[2], splitted[1]))
  for q in queues_unparsed:
    splitted = q.split(',')
    name = splitted[0]
    matches = [x for x in threads if x.name in name]
    if len(matches) != 0:
      matches[0].queues.append(Queue(name.replace('_queue',''), splitted[1], splitted[2]))
  file.seek(0)

def parse_ioc(working_directory):
  ioc_name = find_ioc_file(working_directory)
  if ioc_name is not None:
    ioc = open(working_directory+ioc_name)
    parse_mcu(ioc)
    parse_threads(ioc)
  else:
    raise Exception("IOC file not found in given directory.")

def find_ld_file(working_directory):
  for file_name in os.listdir(working_directory):
    if 'ld' in file_name:
      return file_name

def create_cmake(working_directory):
  global mcu
  linker_script = find_ld_file(working_directory)
  cmake_tpl = Template(filename='templates/CMakeLists.mako', preprocessor=cmake_preprocessor)
  cmake = open(working_directory + 'CMakeLists.txt', 'w')
  abs_path = os.path.abspath(working_directory)
  project_name = os.path.basename(abs_path)
  #TODO: toolchain path
  cmake.write(cmake_tpl.render(mcu_family = mcu.family, mcu_short_name = mcu.short_name, linker_script = linker_script, project_name = project_name))
  cmake.close()

def parse_interrupts_declarations(working_directory):
  global mcu
  interrupts_hpp_file = open(working_directory + "Inc/stm32" + mcu.family.lower() + "xx_it.h")
  handlers = []
  for line in interrupts_hpp_file:
    if 'Handler' in line:
      handlers.append(line[:-1])
  interrupts_hpp_file.close()
  return handlers

def parse_interrupts_definitions(working_directory):
  global mcu
  interrupts_defines_file = open(working_directory + "Src/stm32" + mcu.family.lower() + "xx_it.c")
  handlers = None
  irq_handlers = []
  fault_handlers = []
  opened_brackets = 0
  closed_brackets = 0
  parsing = False
  for line in interrupts_defines_file:
    if parsing and (len(line.strip()) != 0) and 'USER CODE' not in line:
      opened_brackets += line.count('{')
      closed_brackets += line.count('}')
      handlers[-1] = handlers[-1] + line
      if opened_brackets == 0:
        continue
      elif opened_brackets == closed_brackets:
        parsing = False
    elif not parsing:
      if '_Handler' in line:
        fault_handlers.append(line) 
        opened_brackets = line.count('{')
        closed_brackets = line.count('}')
        handlers = fault_handlers
        parsing = True
      elif '_IRQHandler' in line:
        irq_handlers.append(line) 
        opened_brackets = line.count('{')
        closed_brackets = line.count('}')
        handlers = irq_handlers
        parsing = True
  return irq_handlers, fault_handlers

def parse_callbacks(working_directory):
  main_c = open(working_directory + 'Src/main.c')
  callbacks = []
  opened_brackets = 0
  closed_brackets = 0
  parsing = False
  for line in main_c:
    if parsing and (len(line.strip()) != 0) and 'USER CODE' not in line:
      opened_brackets += line.count('{')
      closed_brackets += line.count('}')
      callbacks[-1] = callbacks[-1] + line
      if opened_brackets == 0:
        continue
      elif opened_brackets == closed_brackets:
        parsing = False
    elif not parsing:
      if 'Callback' in line:
        callbacks.append(line)
        opened_brackets = line.count('{')
        closed_brackets = line.count('}')
        parsing = True
  return callbacks

def create_interrupts(working_directory):
  handlers = parse_interrupts_declarations(working_directory) 
  interrupt_hpp_tpl = Template(filename='templates/interrupts_hpp.mako')
  interrupt_hpp = open(working_directory + 'Inc/interrupts.hpp', 'w')
  interrupt_hpp.write(interrupt_hpp_tpl.render(handlers = handlers))
  interrupt_hpp.close()
  irq_handlers, fault_handlers = parse_interrupts_definitions(working_directory)
  callbacks = parse_callbacks(working_directory)
  interrupt_cpp_tpl = Template(filename='templates/interrupts_cpp.mako')
  interrupt_cpp = open(working_directory + 'Src/interrupts.cpp', 'w')
  interrupt_cpp.write(interrupt_cpp_tpl.render(irq_handlers = irq_handlers, fault_handlers = fault_handlers, callbacks = callbacks))
  interrupt_cpp.close()

def parse_gpios(working_directory):
  gpio_define_file = open(working_directory + "Inc/main.h")
  gpios = []
  for line in gpio_define_file:
    if 'GPIO' in line:
      name = line.split(' ')[1]
      if '_Pin' in name:
        pin_name = name.replace('_Pin', '')
        pin = line.split(' ')[2][:-1]
        matches = [x for x in gpios if pin_name == x.name]
        if len(matches) == 0:
          gpios.append(Gpio(pin_name, pin, ""))
        else:
          matches[0].pin = pin
      elif '_GPIO_Port' in name:
        pin_name = name.replace('_GPIO_Port', '') 
        port = line.split(' ')[2][:-1]
        matches = [x for x in gpios if pin_name == x.name]
        if len(matches) == 0:
          gpios.append(Gpio(pin_name, "", port))
        else:
          matches[0].port = port
  return gpios

def parse_peripherals(working_directory):
  main_c = open(working_directory + 'Src/main.c')
  peripherals = []
  peripherals_init = [] 
  peripherals_init_func = []
  opened_brackets = 0
  closed_brackets = 0
  parsing = False
  for line in main_c:
    if parsing and (len(line.strip()) != 0) and 'USER CODE' not in line:
      line = line.replace('Error_Handler', 'error_handler')
      line = line.replace('_Pin','_pin')
      line = line.replace('_GPIO_Port','_port')
      opened_brackets += line.count('{')
      closed_brackets += line.count('}')
      peripherals_init_func[-1] = peripherals_init_func[-1] + line
      if opened_brackets == 0:
        continue
      elif opened_brackets == closed_brackets:
        parsing = False
    elif not parsing:
      match_init = re.search(r'MX_[_a-zA-Z0-9]+\(void\);', line)
      match_peripheral = re.match(r'[a-zA-Z0-9]+_HandleTypeDef\s[a-zA-Z0-9]+', line)
      if match_init is not None:
        init = match_init.group()
        init = init.replace('MX_', '')
        init = init.lower()
        peripherals_init.append(init[:-7])
      elif match_peripheral is not None:
        peripherals.append(match_peripheral.group())
      elif '_Init' in line:
        line = line.replace('static ', '')
        line = line.replace('MX_', 'peripherals::')
        line = line.replace('(void)', '()')
        line = line.lower()
        peripherals_init_func.append(line)
        opened_brackets = line.count('{')
        closed_brackets = line.count('}')
        parsing = True

  return peripherals, peripherals_init, peripherals_init_func

def parse_system_clock_config(working_directory):
  main_c = open(working_directory + 'Src/main.c')
  system_clock_config = ""
  opened_brackets = 0
  closed_brackets = 0
  parsing = False
  for line in main_c:
    if parsing and (len(line.strip()) != 0) and 'USER CODE' not in line:
      line = line.replace('Error_Handler', 'error_handler')
      opened_brackets += line.count('{')
      closed_brackets += line.count('}')
      system_clock_config = system_clock_config + line
      if opened_brackets == 0:
        continue
      elif opened_brackets == closed_brackets:
        parsing = False
    elif not parsing:
      if 'SystemClock_Config(void);' in line:
        continue
      elif 'SystemClock_Config' in line:
        line = line.replace('SystemClock_Config', 'peripherals::system_clock_config')
        line = line.replace('(void)', '()')
        system_clock_config = line
        opened_brackets = line.count('{')
        closed_brackets = line.count('}')
        parsing = True
  return system_clock_config

def parse_msp_init(working_directory):
  global mcu
  hal_msp = open(working_directory+ 'Src/stm32'+ mcu.family.lower() + 'xx_hal_msp.c')
  opened_brackets = 0
  closed_brackets = 0
  parsing = False
  msp_init = []
  for line in hal_msp:
    if parsing and (len(line.strip()) != 0) and 'USER CODE' not in line:
      line = line.replace('_Pin','_pin')
      line = line.replace('_GPIO_Port','_port')
      opened_brackets += line.count('{')
      closed_brackets += line.count('}')
      msp_init[-1] = msp_init[-1] + line
      if opened_brackets == 0:
        continue
      elif opened_brackets == closed_brackets:
        parsing = False
    elif not parsing:
      if ('MspInit' in line) or ('MspDeInit' in line):
        msp_init.append(line)
        opened_brackets = line.count('{')
        closed_brackets = line.count('}')
        parsing = True

  return msp_init

def parse_timebase(working_directory):
  global mcu
  timebase = open(working_directory+ 'Src/stm32'+ mcu.family.lower() + 'xx_hal_timebase_tim.c')
  opened_brackets = 0
  closed_brackets = 0
  parsing = False
  time_peripheral = ""
  timebase_func = []
  for line in timebase:
    if parsing and (len(line.strip()) != 0) and 'USER CODE' not in line:
      opened_brackets += line.count('{')
      closed_brackets += line.count('}')
      timebase_func[-1] = timebase_func[-1] + line
      if opened_brackets == 0:
        continue
      elif opened_brackets == closed_brackets:
        parsing = False
    elif not parsing:
      match_peripheral = re.match(r'[a-zA-Z0-9]+_HandleTypeDef\s+[a-zA-Z0-9]+', line)
      if match_peripheral is not None:
        time_peripheral = match_peripheral.group()
      if ('HAL_InitTick' in line) or ('HAL_SuspendTick' in line) or ('HAL_ResumeTick' in line):
        timebase_func.append(line)
        opened_brackets = line.count('{')
        closed_brackets = line.count('}')
        parsing = True

  return time_peripheral, timebase_func


def create_peripherals(working_directory):
  global mcu
  gpios = parse_gpios(working_directory)
  peripherals, peripherals_init, peripherals_init_func = parse_peripherals(working_directory)
  time_peripheral, timebase_func = parse_timebase(working_directory)
  peripherals.append(time_peripheral)
  peripherals_hpp_tpl = Template(filename="templates/peripherals_hpp.mako")
  peripherals_hpp = open(working_directory + 'Inc/peripherals.hpp', 'w')
  peripherals_hpp.write(peripherals_hpp_tpl.render(mcu_family=mcu.family.lower(), 
                                                   gpios = gpios,
                                                   peripherals = peripherals, 
                                                   peripherals_init = peripherals_init))
  peripherals_hpp.close()


  system_clock_config = parse_system_clock_config(working_directory)
  msp_init = parse_msp_init(working_directory)  
  peripherals_cpp_tpl = Template(filename='templates/peripherals_cpp.mako')  
  peripherals_cpp = open(working_directory + 'Src/peripherals.cpp', 'w')
  peripherals_cpp.write(peripherals_cpp_tpl.render(peripherals = peripherals,
                                                   peripherals_init = peripherals_init,
                                                   peripherals_init_func = peripherals_init_func,
                                                   msp_init = msp_init,
                                                   system_clock_config = system_clock_config,
                                                   timebase_func = timebase_func))
  peripherals_cpp.close()

def remove_files(working_directory):
  os.remove(working_directory + 'Inc/stm32' + mcu.family.lower() + 'xx_it.h')
  os.remove(working_directory + 'Inc/main.h')
  os.remove(working_directory + 'Src/stm32' + mcu.family.lower() + 'xx_it.c')
  os.remove(working_directory + 'Src/stm32' + mcu.family.lower() + 'xx_hal_msp.c')
  os.remove(working_directory + 'Src/stm32' + mcu.family.lower() + 'xx_hal_timebase_tim.c')
  os.remove(working_directory + 'Src/freertos.c')
  os.remove(working_directory + 'Src/main.c')

def create_threads(working_directory):
  global threads
  for t in threads:
    thread_cpp = open(working_directory+'Src/'+t.name+'_thread.cpp', 'w')
    thread_hpp = open(working_directory+'Inc/'+t.name+'_thread.hpp', 'w')
    thread_cpp_tmp = Template(filename='templates/thread_cpp.mako')
    thread_hpp_tmp = Template(filename='templates/thread_hpp.mako')
    thread_cpp.write(thread_cpp_tmp.render(thread = t)) 
    thread_hpp.write(thread_hpp_tmp.render(thread = t)) 
    thread_cpp.close()
    thread_hpp.close()

def create_main(working_directory):
  global threads
  main_cpp = open(working_directory+"Src/main.cpp", 'w')
  main_cpp_tmp = Template(filename='templates/main_cpp.mako')
  main_cpp.write(main_cpp_tmp.render(threads = threads))
  main_cpp.close()

def copy_templates(working_directory):
  copyfile(working_directory+'templates/mem_ptr.hpp', working_directory+'Inc/mem_ptr.hpp')
  copyfile(working_directory+'templates/thread_base.hpp', working_directory+'Inc/thread_base.hpp')

def cleanup(working_directory):
  rmtree(working_directory+'templates')   

def download_template(working_directory):
  try:
    os.mkdir(working_directory+'templates')
  except FileExistsError:
    pass
  url = 'https://gitlab.com/pszemoc/cubemx-garbage-cleaner/raw/master/templates/'
  for file in template_files:
    print('Downloading ' + file)
    with urllib.request.urlopen(url + file) as response, open(working_directory+'templates/'+file,'wb') as out_file:
      copyfileobj(response, out_file)
  

def main():
  args = parser.parse_args()
  working_directory = args.path
  try:
    parse_ioc(working_directory)
    download_template(working_directory)
    remove_project_files(working_directory)
    create_cmake(working_directory)
    create_interrupts(working_directory)
    create_peripherals(working_directory)
    remove_files(working_directory)
    create_threads(working_directory)
    create_main(working_directory)
    copy_templates(working_directory)
    cleanup(working_directory)
    subprocess.run('clang-format -i -style=llvm ' + working_directory + 'Src/* ' + working_directory + 'Inc/*', shell=True, check=True)
  except Exception as ex:
    print(ex.args[0])

if __name__ == '__main__':
  main()
