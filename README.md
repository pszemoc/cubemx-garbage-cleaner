# Cube MX Garbage Cleaner
## Dependencies
```
sudo apt-get install python3 python3-mako clang-format
```
## Usage
1. Create project with STM32CubeMX.
2. In Project Manager tab:
  - Check 'Do not generate the main()'
  - Change Toolchain/IDE to SW4STM32
3. Generate code.
4. Run:
```
python3 cubemx-garbage-cleaner.py 'path-to-cubemx-project'
```

## FreeRTOS integration

Tasks with name 'task_name' are converted to classes 'task_name_thread_t'.

Queues with name contained in any 'task_name' are declared inside given thread class.
